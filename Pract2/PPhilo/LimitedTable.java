// CSD Mar 2013 Juansa Sendra
 
public class LimitedTable extends RegularTable { //max 4 in dinning-room
	int capacity = 0;
	final static int MAX_CAPACITY = 4;
	public LimitedTable(Log log) {super(log);}
	public synchronized void enter(int id) {
		while(MAX_CAPACITY < capacity+1) {
			log.wenter(id);
			waiting();
		}
		capacity++;
		notifyAll();		
		log.enter(id);
		
	}
	public synchronized void exit(int id) {		
		capacity--;
		notifyAll();
		log.exit(id);
	}
}
