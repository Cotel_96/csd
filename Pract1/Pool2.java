// CSD feb 2015 Juansa Sendra

 

public class Pool2 extends Pool0{ //max kids/instructor
	// To be completed
	private int kidsSwimming;
	private int insSwimming;

	public void make(Log log) {
		super.make(log);
		kidsSwimming = 0;
		insSwimming = 0;
	}

	private void espera() {
		try {
			wait();
		} catch (Exception e) {
			
		}
	}

	public synchronized long kidSwims(int id){
        while(insSwimming == 0 || (log.maxKI()*insSwimming) < (kidsSwimming + 1)){
            log.enterWait(id);
            espera();
        }
        kidsSwimming++;
        notifyAll();
        return log.swims(id);
    }
    
    public synchronized long kidRests(int id){
        kidsSwimming--;
        notifyAll();
        return super.kidRests(id);
    }
    
    public synchronized long instructorSwims(int id){
        insSwimming++;
        return super.instructorSwims(id);
    }
    
    public synchronized long instructorRests(int id){
        while((kidsSwimming != 0 && insSwimming == 1) || log.maxKI()*(insSwimming-1) < kidsSwimming){
            log.leaveWait(id);
            espera();
        }
        insSwimming--;
        notifyAll();
        return super.instructorRests(id);
    }

}