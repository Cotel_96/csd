// CSD feb 2015 Juansa Sendra

 
public class Pool1 extends Pool0 {//there cannot be kids alone
	// To be completed
	private int kidsSwimming;
	private int insSwimming;

	@Override
	public void make(Log log0) {
		super.make(log0);
		kidsSwimming = 0;
		insSwimming = 0;
	}

	private void espera() {
		try {
			wait();
		} catch (Exception e) {
			
		}
	}

	@Override
	public synchronized long kidSwims(int id) {
		while(insSwimming == 0) {
			log.enterWait(id);
			espera();
		}
		kidsSwimming++;
		notifyAll();
		return log.swims(id);
	}

	@Override
	public synchronized long kidRests(int id) {
		kidsSwimming--;
		notifyAll();
		return super.kidRests(id);
	}

	@Override
	public synchronized long instructorSwims(int id) {
		insSwimming++;
		return super.instructorSwims(id);
	}

	@Override
	public synchronized long instructorRests(int id) {
		while(kidsSwimming != 0 && insSwimming == 1) {
			log.leaveWait(id);
			espera();
		}
		insSwimming--;
		notifyAll();
		return super.instructorRests(id);
	}
}